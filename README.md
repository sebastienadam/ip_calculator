# Calculateur de sous-réseaux IP (v4)

Cet outil permet de calculer des sous-réseau sur base d'une adresse réseau donné
et d'un masque de réseau. Le calcul peut se faire sur base d'un nombre de
sous-réseaux, de hôtes par réseau ou de sous-réseaux de tailles variables.

Vous pouvez le voir fonctionner ici:

http://www.sebastienadam.be/ipcalculator/