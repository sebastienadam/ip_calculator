/*
 * Copyright (c) 2016 Sébastien Adam - sebastien.adam.webdev@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

var versionNumber = "1.0.0";

/**
 * Convert an IP addres into its corresponding integer value. We assume that the
 * given address is valid
 * @param {Array} arrAddress The address to be converted
 * @returns {Number} The interger representation of the address
 */
function addressToInt(arrAddress) {
  return arrAddress[0] * 16777216 + arrAddress[1] * 65536 + arrAddress[2] * 256 + arrAddress[3];
}

/**
 * Convert an IP address into string. We assume that the given address is valid
 * @param {Array} arrAddress The address to be converted
 * @returns {String} The string representation of the address
 */
function addressToString(arrAddress) {
  return arrAddress.join('.');
}

/**
 * Convert an IP address into a binary string. We assume that the given address
 * is valid
 * @param {Array} arrAddress The address to be converted
 * @returns {String} The converted address
 */
function addressToStringBinary(arrAddress) {
  var strTmp;
  var zeroFill = "00000000";
  var result = new Array(4);
  for (var i = 0; i < arrAddress.length; ++i) {
    strTmp = zeroFill + arrAddress[i].toString(2);
    result[i] = strTmp.slice(-zeroFill.length);
  }
  return result.join('.');
}

/**
 * Add a number of hosts for a variable subnet.
 * @returns {Boolean}
 */
function addVariable() {
  if (!validateVariableNumberHosts()) {
    return false;
  }
  if (!validateMainMask()) {
    return false;
  }
  var askedHosts = parseInt($('#hostsVariable').val(), 10);
  var mainMask = stringToAddress($('#networkMask').val());
  var arrSubnetMask = getMaskByHostsNumber(askedHosts, mainMask);
  var neededHosts = getMaxHosts(arrSubnetMask);
  $('#hostsVariableList').append('<li><span class="varasked">' + askedHosts + '</span> hôtes (<span class="varneeded">' + neededHosts + '</span> hôtes requis, masque <span class="varmask">' + addressToString(arrSubnetMask) + ')</span></li>');
}

/**
 * Generate the subnetworks based on the number of hosts.
 */
function divideByHosts() {
  $('#result').text('');
  if (validateMainIPAddress()) {
    var mainAddr = stringToAddress($('#ipAddress').val());
  } else {
    return false;
  }
  if (validateMainMask()) {
    var mainMask = stringToAddress($('#networkMask').val());
  } else {
    return false;
  }
  if (validateNumberHosts()) {
    var askedHostsNumber = parseInt($('#hostsNumber').val(), 10);
  } else {
    return false;
  }
  var subnetMask = getMaskByHostsNumber(askedHostsNumber);
  $('#result').append('<p>Nouveau masque réseau: ' + addressToString(subnetMask) + ' (' + maskAddressToCIDR(subnetMask) + ')</p>');
  var nbSubnet = getNumberSubnet(mainMask, subnetMask);
  $('#result').append('<p>Nombre de sous-réseaux: ' + nbSubnet + '</p>');
  var networkAddress = getNetworkAddress(mainAddr, mainMask);
  if (nbSubnet > 128) {
    $('#result').append('<div class="alert alert-danger" role="alert"><p>Il y a trop de sous-réseaux pour les afficher tous...</p></div>');
    nbSubnet = 128;
  }
  var text = '';
  text += '<div class="row">';
  for (var i = 0; i < nbSubnet; i++) {
    text += '<div class="col-md-3">';
    text += '<h4>Réseau ' + (i + 1) + '</h4>';
    text += printNetworkDetail(networkAddress, subnetMask);
    text += '</div>';
    networkAddress = getNextNetworkAddress(networkAddress, subnetMask);
  }
  text += '</div>';
  $('#result').append(text);
}

/**
 * Generate the subnetworks based on the number of subnets.
 */
function divideBySubnets() {
  $('#result').text('');
  if (validateMainIPAddress()) {
    var mainAddr = stringToAddress($('#ipAddress').val());
  } else {
    return false;
  }
  if (validateMainMask()) {
    var mainMask = stringToAddress($('#networkMask').val());
  } else {
    return false;
  }
  if (validateNumberSubnets()) {
    var askedSubnetsNumber = parseInt($('#subnetsNumber').val(), 10);
  } else {
    return false;
  }
  var subnetMask = getMaskBySubnetsNumber(askedSubnetsNumber, mainMask);
  $('#result').append('<p>Nouveau masque réseau: ' + addressToString(subnetMask) + ' (' + maskAddressToCIDR(subnetMask) + ')</p>');
  var nbSubnet = getNumberSubnet(mainMask, subnetMask);
  $('#result').append('<p>Nombre de sous-réseaux: ' + nbSubnet + '</p>');
  var networkAddress = getNetworkAddress(mainAddr, mainMask);
  if (nbSubnet > 128) {
    $('#result').append('<div class="alert alert-danger" role="alert"><p>Il y a trop de sous-réseaux pour les afficher tous...</p></div>');
    nbSubnet = 128;
  }
  var text = '';
  text += '<div class="row">';
  for (var i = 0; i < nbSubnet; i++) {
    text += '<div class="col-md-3">';
    text += '<h4>Réseau ' + (i + 1) + '</h4>';
    text += printNetworkDetail(networkAddress, subnetMask);
    text += '</div>';
    networkAddress = getNextNetworkAddress(networkAddress, subnetMask);
  }
  text += '</div>';
  $('#result').append(text);
}

/**
 * Generate the subnetworks based on the variable number of hosts.
 */
function divideVariable() {
  $('#result').text('');
  if (validateMainIPAddress()) {
    var mainAddr = stringToAddress($('#ipAddress').val());
  } else {
    return false;
  }
  if (validateMainMask()) {
    var mainMask = stringToAddress($('#networkMask').val());
  } else {
    return false;
  }
  var arrAskedHosts = getVariableHostsList();
  var nbSubnet = arrAskedHosts.length;
  $('#result').append('<p>Nombre de sous-réseaux: ' + nbSubnet + '</p>');
  var networkAddress = getNetworkAddress(mainAddr, mainMask);
  text += '<div class="row">';
  var text = '';
  var subnetMask;
  for (var i = 0; i < arrAskedHosts.length; i++) {
    subnetMask = getMaskByHostsNumber(arrAskedHosts[i]);
    text += '<div class="col-md-3">';
    text += '<h4>Réseau ' + (i + 1) + '</h4>';
    text += printNetworkDetail(networkAddress, subnetMask);
    text += '</div>';
    networkAddress = getNextNetworkAddress(networkAddress, subnetMask);
  }
  text += '</div>';
  $('#result').append(text);
}

/**
 * Get the class of an address. We assume that given the address is valid
 * @param {Array} arrAddress The address that need to find the class. We assume
 *        that the address is valid
 * @returns {String} The class of the address
 */
function getAddressClass(arrAddress) {
  if (arrAddress[0] < 128)
    return "A";
  else if (arrAddress[0] < 192)
    return "B";
  else if (arrAddress[0] < 224)
    return "C";
  else if (arrAddress[0] < 240)
    return "D";
  else
    return "E";
}

/**
 * Give the broadcast address of a network.
 * @param {Array} arrAddress The network address
 * @param {Array} arrMask The network mask
 * @returns {Array} The broadcast address of the network
 */
function getBroadcastAddress(arrAddress, arrMask) {
  var arrBroadcastAddress = new Array(4);
  for (i = 0; i < arrAddress.length; ++i) {
    arrBroadcastAddress[i] = arrAddress[i] | (arrMask[i] ^ 255);
  }
  return arrBroadcastAddress;
}

/**
 * Get the default network mask of an address depending of the class of the
 * address
 * @param {type} arrAddress The address that need to find the network mask
 * @returns {Array} The default network mask
 */
function getDefaultMask(arrAddress) {
  var ipClass = getAddressClass(arrAddress);
  switch (ipClass) {
    case "A":
      return new Array(255, 0, 0, 0);
      break;
    case "B":
      return  new Array(255, 255, 0, 0);
      break;
    case "C":
      return  new Array(255, 255, 255, 0);
      break;
    default:
      return  new Array(255, 255, 255, 255);
  }
}
/**
 * Give the first host address of a network
 * @param {Array} arrNetworkAddress The network address
 * @param {Array} arrMask The network mask
 * @returns {Array} The address of the first host
 */
function getFisrtHostAddress(arrNetworkAddress, arrMask) {
  var arrFirstHost = new Array(4);
  for (var i = 0; i < arrNetworkAddress.length; i++) {
    arrFirstHost[i] = arrNetworkAddress[i];
  }
  arrFirstHost[3]++;
  return arrFirstHost;
}

/**
 * Give the last host address of a network
 * @param {Array} arrNetworkAddress The network address
 * @param {Array} arrMask The network mask
 * @returns {Array} The address of the last host
 */
function getLastHostAddress(arrNetworkAddress, arrMask) {
  var arrLastHost = getBroadcastAddress(arrNetworkAddress, arrMask);
  arrLastHost[3]--;
  return arrLastHost;
}

/**
 * Give the subnet mask corresponding to the given number of guests.
 * @param {Interger} nbHosts The minimal number of hosts for the network
 * @returns {Array} The network mask corresponding with the number of hosts.
 */
function getMaskByHostsNumber(nbHosts) {
  var nbBitsHosts = 2;
  while ((Math.pow(2, nbBitsHosts) - 2) < nbHosts) {
    nbBitsHosts++;
  }
  var strBinMask = Array(33 - nbBitsHosts).join('1') + Array(nbBitsHosts + 1).join('0');
  var intMask = parseInt(strBinMask, 2);
  return intToAddress(intMask);
}

/**
 * Give the subnet mask corresponding to the given number of subnets.
 * @param {Integer} nbSubnets The minimal number of subnets
 * @param {Array} arrMask The actual net mask
 * @returns {Array} The subnet mask
 */
function getMaskBySubnetsNumber(nbSubnets, arrMask) {
  var nbBitsSubnet = 0;
  while (Math.pow(2, nbBitsSubnet) < nbSubnets) {
    nbBitsSubnet++;
  }
  var strBinNetMask = addressToStringBinary(arrMask);
  var nbBitsNetMask = (strBinNetMask.match(/1/g) || []).length;
  var nbBitsSubnetMask = nbBitsNetMask + nbBitsSubnet;
  var strBinSubnetMask = Array(nbBitsSubnetMask + 1).join('1') + Array(33 - nbBitsSubnetMask).join('0');
  var intSubnetMask = parseInt(strBinSubnetMask, 2);
  return intToAddress(intSubnetMask);
}

/**
 * Gives the maximum number of hosts for a given network mask.
 * @param {Array} arrMask The given network mask.
 * @returns {Number} The maximum number of hosts for the given network mask.
 */
function getMaxHosts(arrMask) {
  var strMask = addressToStringBinary(arrMask);
  var nbBitsHosts = (strMask.match(/0/g) || []).length;
  return Math.pow(2, nbBitsHosts) - 2;
}

/**
 * Gives the maximum number of subnets for a given network mask.
 * @param {Array} arrMask The given network mask.
 * @returns {Number} The maximum number of hosts for the given network mask.
 */
function getMaxSubnets(arrMask) {
  var strMask = addressToStringBinary(arrMask);
  var nbBitsHosts = (strMask.match(/0/g) || []).length;
  return Math.pow(2, nbBitsHosts - 2);
}

/**
 * Give the address of a network based on an IP address and a network mask.
 * @param {type} arrAddress The IP address in the network.
 * @param {type} arrMask The nework mask.
 * @returns {Array} The network address.
 */
function getNetworkAddress(arrAddress, arrMask) {
  var arrNetwork = new Array(4);
  for (var i = 0; i < arrAddress.length; i++) {
    arrNetwork[i] = arrAddress[i] & arrMask[i];
  }
  return arrNetwork;
}

/**
 * Give the address of the next network
 * @param {Array} arrNetworkAddress The address of the current network
 * @param {Array} arrMask The mask of the current network
 * @returns {Array} The address of the next network
 */
function getNextNetworkAddress(arrNetworkAddress, arrMask) {
  var arrNextNetwork = getBroadcastAddress(arrNetworkAddress, arrMask);
  var intNextNetwork = addressToInt(arrNextNetwork);
  intNextNetwork++;
  arrNextNetwork = intToAddress(intNextNetwork);
  return arrNextNetwork;
}

/**
 * Give the number of sub network to the main network.
 * @param {Array} arrNetMask The main network mask.
 * @param {Array} arrSubnetMask The subnet mask.
 * @returns {Number} The number of subnet in the main network.
 */
function getNumberSubnet(arrNetMask, arrSubnetMask) {
  var strNetCidr = maskAddressToCIDR(arrNetMask);
  var intNetCird = parseInt(strNetCidr.replace('/', ''), 10);
  var strSubnetCidr = maskAddressToCIDR(arrSubnetMask);
  var intSubnetCird = parseInt(strSubnetCidr.replace('/', ''), 10);
  return Math.pow(2, intSubnetCird - intNetCird);
}

/**
 * Give the list of the asked number of hosts for variable subnets in descending
 * order.
 * @returns {Array} The list of the asked number of hosts by variable subnet
 */
function getVariableHostsList() {
  var arrHosts = new Array();
  $('#hostsVariableList li span.varasked').each(function () {
    arrHosts.push(parseInt($(this).text(), 10));
  });
  arrHosts.sort(function(a, b){return b-a});
  return arrHosts;
}

/**
 * Convert the integer value to its corresponding address
 * @param {type} intAddress The interger value to convert
 * @returns {Array} The converted address
 */
function intToAddress(intAddress) {
  var arrAddress = new Array();
  while (intAddress > 0) {
    arrAddress.unshift(intAddress % 256);
    intAddress = (intAddress - intAddress % 256) / 256;
  }
  while (arrAddress.length !== 4) {
    arrAddress.unshift(0);
  }
  return arrAddress;
}

/**
 * Verify that the given address is a valid IP address
 * @param {type} arrAddress The address to be checked
 * @returns {Boolean} 'true' if the address is valid, or 'false' if it's not
 */
function isValidAdress(arrAddress) {
  if (Object.prototype.toString.call(arrAddress) !== '[object Array]') {
    return false;
  }
  if (arrAddress.length !== 4) {
    return false;
  }
  for (var i = 0; i < arrAddress.length; ++i) {
    if ((arrAddress[i] !== parseInt(arrAddress[i], 10)) || arrAddress[i] < 0 || arrAddress[i] > 255) {
      return false;
    }
  }
  return true;
}

/**
 * Verify that the given mask address is a valid IP mask
 * @param {Array} arrMask The mask to be checked
 * @returns {Boolean} 'true' if the mask is valid, or 'false' if it's not
 */
function isValidMaskAddress(arrMask) {
  var prev = 255;
  if (!isValidAdress(arrMask)) {
    return false;
  }
  for (var i = 0; i < arrMask.length; ++i) {
    switch (arrMask[i]) {
      case 255:
      case 254:
      case 252:
      case 248:
      case 240:
      case 224:
      case 192:
      case 128:
        if (prev !== 255)
          return false;
      case 0:
        break;
      default:
        return false;
    }
    prev = arrMask[i];
  }
  return true;
}

/**
 * Verify that the given mask in CIDR notation is a valid IP mask
 * @param {String} strMask The mask to be checked
 * @returns {Boolean} 'true' if the mask is valid, or 'false' if it's not
 */
function isValidMaskCIDR(strMask) {
  var res = strMask.match(/^\/(\d+)$/);
  if ((res !== null) && (res.length === 2) && (res[1] <= 32)) {
    return true;
  }
  return false;
}

/**
 * Convert a address mask into its CIRD notation
 * @param {Array} arrAddress The mask to be converted
 * @returns {String} The mask in CIRD notation
 */
function maskAddressToCIDR(arrAddress) {
  var strAddress = addressToStringBinary(arrAddress);
  var result = '/' + (strAddress.match(/1/g) || []).length;
  return result;
}

/**
 * Converts a network mask in CIDR notation to his address.
 * @param {type} strMask The network mask in CIDR notation
 * @returns {Array|intToAddress.arrAddress}
 */
function maskCIDRToAddress(strMask) {
  var maskLen = parseInt(strMask.match(/^\/(\d+)$/)[1], 10);
  var strBinMask = Array(maskLen + 1).join('1') + Array(33 - maskLen).join('0');
  var intMask = parseInt(strBinMask, 2);
  return intToAddress(intMask);
}

/**
 * Displays the details of a network (network address, first and last host
 * address, broadcast address).
 * @param {type} arrNetworkAddress The address of the network
 * @param {type} arrMask The mask of the network
 */
function printNetworkDetail(arrNetworkAddress, arrMask) {
  var arrFisrtHost = getFisrtHostAddress(arrNetworkAddress, arrMask);
  var arrLastHost = getLastHostAddress(arrNetworkAddress, arrMask);
  var arrBroadcast = getBroadcastAddress(arrNetworkAddress, arrMask);
  var text = "";
  text += '<p>Masque réseau: <strong>' + addressToString(arrMask) + '</strong></p>';
  text += '<p>Addresse réseau: <strong>' + addressToString(arrNetworkAddress) + '</strong></p>';
  text += '<p>Adresse du premier hôte: <strong>' + addressToString(arrFisrtHost) + '</strong></p>';
  text += '<p>Adresse du dernier hôte: <strong>' + addressToString(arrLastHost) + '</strong></p>';
  text += '<p>Adresse de diffusion: <strong>' + addressToString(arrBroadcast) + '</strong></p>';
  text += "<p>Nombre maximal d'hôtes: <strong>" + getMaxHosts(arrMask) + "</strong></p>";
  return text;
}

/**
 * Set the default network mask when the user type an address. If a mask is
 * already set, don't change anything
 * @param {type} mainAddr The address typed by the user
 */
function setMainDefaultMask(mainAddr) {
  if ($('#networkMask').val() === '') {
    var mainMask = getDefaultMask(mainAddr);
    $('#networkMask').val(addressToString(mainMask));
    validateMainMask();
  }
}

/**
 * Add a tooltip to an element
 * @param {String} selector The selector of the element(s) on which we must add
 *        the tooltip
 * @param {String} text Tooltip text to add
 */
function setTooltip(selector, text) {
  $(selector).tooltip('hide')
          .attr('data-original-title', text)
          .tooltip('fixTitle')
          .tooltip('show');
}

/**
 * Converts a string to an IP address.
 * @param {String} strAddress The string to be converted.
 * @returns {Array|Boolean} 'false' if the string is not a valid IP address, or
 *        an array with the 4 bytes of the address.
 */
function stringToAddress(strAddress) {
  var arrAddress;
  if ((typeof strAddress) !== 'string') {
    return false;
  }
  if (strAddress === '') {
    return false;
  }
  arrAddress = strAddress.split('.');
  for (var i = 0; i < arrAddress.length; ++i) {
    arrAddress[i] = parseInt(arrAddress[i], 10);
  }
  if (!isValidAdress(arrAddress)) {
    return false;
  }
  return arrAddress;
}

/**
 * Validate the IP address typed by the user
 * @returns {Boolean} 'true' the IP address typed by the user is valid. 'false'
 *        otherwise
 */
function validateMainIPAddress() {
  var mainAddr = stringToAddress($('#ipAddress').val());
  if (mainAddr === false) {
    $('#ipAddressContainer').removeClass('has-success').addClass('has-error');
    $('#ipAddressHelpBlock').text("L'adresse introduite n'est pas valide");
    return false;
  }
  $('#ipAddressContainer').removeClass('has-error').addClass('has-success');
  $('#ipAddressHelpBlock').text('');
  setMainDefaultMask(mainAddr);
  return true;
}

/**
 * Validate the mask typed by the user
 * @returns {Boolean} 'true' the mask typed by the user is valid. 'false'
 *        otherwise
 */
function validateMainMask() {
  var mainMask = stringToAddress($('#networkMask').val());
  if (isValidMaskAddress(mainMask)) {
    $('#networkMaskContainer').removeClass('has-error').addClass('has-success');
    $('#networkMaskHelpBlock').text('');
    setTooltip('#networkMask', maskAddressToCIDR(mainMask));
    return true;
  }
  var tmpMask = $(this).val();
  if (isValidMaskCIDR(tmpMask)) {
    $('#networkMaskContainer').removeClass('has-error').addClass('has-success');
    $('#networkMaskHelpBlock').text('');
    setTooltip('#networkMask', tmpMask);
    mainMask = maskCIDRToAddress(tmpMask);
    $(this).val(addressToString(mainMask));
    return true;
  }
  $('#networkMaskContainer').removeClass('has-success').addClass('has-error');
  $('#networkMaskHelpBlock').text("Le masque introduit n'est pas valide");
  return false;
}

/**
 * Validates the number of hosts given by the user.
 * @returns {Boolean} 'true' the number of hosts typed by the user is valid.
 *        'false' otherwise
 */
function validateNumberHosts() {
  var askedHostsNumber = parseInt($('#hostsNumber').val(), 10);
  if (isNaN(askedHostsNumber)) {
    $('#hostsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#hostsNumberHelpBlock').text("Le nombre d'hôtes introduit n'est pas valide");
    return false;
  }
  if (askedHostsNumber < 1) {
    $('#hostsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#hostsNumberHelpBlock').text("Vous devez donner un nombre positif");
    return false;
  }
  var mainMask = stringToAddress($('#networkMask').val());
  if (!isValidMaskAddress(mainMask)) {
    $('#hostsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#hostsNumberHelpBlock').text("Le masque introduit n'est pas valide");
    return false;
  }
  var maxValue = getMaxHosts(mainMask);
  if (askedHostsNumber > maxValue) {
    $('#hostsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#hostsNumberHelpBlock').text("Le nombre d'hôtes demandé est suppérieur au nombre maximum d'hôtes (" + maxValue + ")");
    return false;
  }
  $('#hostsNumberContainer').removeClass('has-error').addClass('has-success');
  $('#hostsNumberHelpBlock').text('');
  return true;
}

/**
 * Validates the number of subnets given by the user.
 * @returns {Boolean} 'true' the number of subnets typed by the user is valid.
 *        'false' otherwise
 */
function validateNumberSubnets() {
  var askedSubnetsNumber = parseInt($('#subnetsNumber').val(), 10);
  if (isNaN(askedSubnetsNumber)) {
    $('#subnetsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#subnetsNumberHelpBlock').text("Le nombre de sous-réseaux introduit n'est pas valide");
    return false;
  }
  if (askedSubnetsNumber < 1) {
    $('#subnetsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#subnetsNumberHelpBlock').text("Vous devez donner un nombre positif");
    return false;
  }
  var mainMask = stringToAddress($('#networkMask').val());
  if (!isValidMaskAddress(mainMask)) {
    $('#subnetsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#subnetsNumberHelpBlock').text("Le masque introduit n'est pas valide");
    return false;
  }
  var maxValue = getMaxSubnets(mainMask);
  if (askedSubnetsNumber > maxValue) {
    $('#subnetsNumberContainer').removeClass('has-success').addClass('has-error');
    $('#subnetsNumberHelpBlock').text("Le nombre de sous-réseaux demandé est suppérieur au nombre maximum de sous-réseaux (" + maxValue + ")");
    return false;
  }
  $('#subnetsNumberContainer').removeClass('has-error').addClass('has-success');
  $('#subnetsNumberHelpBlock').text('');
  return true;
}

/**
 * Validates the number of hosts given by the user for a variable subnet.
 * @returns {Boolean} 'true' the number of hosts typed by the user is valid.
 *        'false' otherwise
 */
function validateVariableNumberHosts() {
  var askedHosts = parseInt($('#hostsVariable').val(), 10);
  if (isNaN(askedHosts)) {
    $('#hostsVariableContainer').removeClass('has-success').addClass('has-error');
    $('#hostsVariableHelpBlock').text("Le nombre d'hôtes introduit n'est pas valide");
    return false;
  }
  if (askedHosts < 1) {
    $('#hostsVariableContainer').removeClass('has-success').addClass('has-error');
    $('#hostsVariableHelpBlock').text("Vous devez donner un nombre positif");
    return false;
  }
  var mainMask = stringToAddress($('#networkMask').val());
  if (!isValidMaskAddress(mainMask)) {
    $('#hostsVariableContainer').removeClass('has-success').addClass('has-error');
    $('#hostsVariableHelpBlock').text("Le masque introduit n'est pas valide");
    return false;
  }
  var maxValue = getMaxHosts(mainMask);
  var allAskedHosts = 0;
  $('#hostsVariableList li span.varneeded').each(function () {
    allAskedHosts += parseInt($(this).text(), 10) + 2;
  });
  allAskedHosts += getMaxHosts(getMaskByHostsNumber(askedHosts));
  if (allAskedHosts > maxValue) {
    $('#hostsVariableContainer').removeClass('has-success').addClass('has-error');
    $('#hostsVariableHelpBlock').text("Le nombre d'hôtes demandé (" + allAskedHosts + ") est suppérieur au nombre maximum d'hôtes (" + maxValue + ")");
    return false;
  }
  $('#hostsVariableContainer').removeClass('has-error').addClass('has-success');
  $('#hostsVariableHelpBlock').text('');
  return true;
}

$(function () {
  $('#ipAddress').blur(validateMainIPAddress);
  $('#networkMask').blur(validateMainMask);
  $('#hostsNumber').blur(validateNumberHosts);
  $('#subnetsNumber').blur(validateNumberSubnets);
  $('#hostsVariable').blur(validateVariableNumberHosts);
  $('#divideByHosts').click(divideByHosts);
  $('#divideByNets').click(divideBySubnets);
  $('#addVariable').click(addVariable);
  $('#clearVariableList').click(function () {
    $('#hostsVariableList').text('');
    validateVariableNumberHosts();
  });
  $('#deleteLastVariableList').click(function () {
    $('#hostsVariableList li:last').remove();
    validateVariableNumberHosts();
  });
  $('#divideVariable').click(divideVariable);
  $('#versionNumber').text(versionNumber);
});